package cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.model;

import java.util.List;

public class Personas {
    private List<Persona> miPersonas;

    public Personas(List<Persona> miPersonas) {
        this.miPersonas = miPersonas;
    }

    public List<Persona> getPersonas() {
        return miPersonas;
    }
}
