package cr.ac.ucr.ecci.eseg.miexamen02.personas.interactor;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.exceptions.CantRetrieveItemsException;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.model.Persona;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.repository.PersonasRepository;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.repository.PersonasRepositoryImpl;

public class PersonasInteractorImpl implements PersonasInteractor {

    private PersonasRepository personasRepository = new PersonasRepositoryImpl();

    OnProgressUpdateListener onProgressUpdateListener;
    OnFinishedListener onFinishedListener;

    public PersonasInteractorImpl() {
        ((PersonasRepositoryImpl) personasRepository).setOnProgressUpdateListener(this);
        ((PersonasRepositoryImpl) personasRepository).setOnFinishedListener(this);
    }

    public void setOnProgressUpdateListener(OnProgressUpdateListener onProgressUpdateListener) {
        this.onProgressUpdateListener = onProgressUpdateListener;
    }

    public void setOnFinishedListener(OnFinishedListener onFinishedListener) {
        this.onFinishedListener = onFinishedListener;
    }

    @Override
    public void getItems() {
        try {
            personasRepository.obtainItems();
        } catch (CantRetrieveItemsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(int progress) {
        onProgressUpdateListener.onProgressUpdate(progress);
    }

    @Override
    public void onFinished(List<Persona> personas) {
        onFinishedListener.onFinished(personas);
    }
}

