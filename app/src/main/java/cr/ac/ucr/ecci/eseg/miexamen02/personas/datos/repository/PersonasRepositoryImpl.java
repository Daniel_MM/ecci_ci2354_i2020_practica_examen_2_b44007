package cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.repository;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.exceptions.CantRetrieveItemsException;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.model.Persona;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.service.ServiceDataSource;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.service.ServiceDataSourceImpl;

public class PersonasRepositoryImpl implements PersonasRepository {

    ServiceDataSource serviceDataSource = new ServiceDataSourceImpl();

    OnProgressUpdateListener onProgressUpdateListener;
    OnFinishedListener onFinishedListener;

    public PersonasRepositoryImpl() {
        ((ServiceDataSourceImpl) serviceDataSource).setOnProgressUpdateListener(this);
        ((ServiceDataSourceImpl) serviceDataSource).setOnFinishedListener(this);
    }

    public void setOnProgressUpdateListener(OnProgressUpdateListener onProgressUpdateListener) {
        this.onProgressUpdateListener = onProgressUpdateListener;
    }

    public void setOnFinishedListener(OnFinishedListener onFinishedListener) {
        this.onFinishedListener = onFinishedListener;
    }

    @Override
    public void obtainItems() throws CantRetrieveItemsException {
        try {
            ((ServiceDataSourceImpl) serviceDataSource).execute();
        } catch (Exception e) {
            throw new CantRetrieveItemsException(e.getMessage());
        }
    }


    public void onProgressUpdate(int progress) {
        onProgressUpdateListener.onProgressUpdate(progress);
    }

    public void onFinished(List<Persona> personas) {
        onFinishedListener.onFinished(personas);
    }
}
