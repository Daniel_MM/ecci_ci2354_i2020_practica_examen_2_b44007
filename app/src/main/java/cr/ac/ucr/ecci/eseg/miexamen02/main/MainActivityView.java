package cr.ac.ucr.ecci.eseg.miexamen02.main;

import java.util.ArrayList;
import java.util.HashMap;

import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.model.Persona;

public interface MainActivityView {
    void showProgress();

    void updateProgress(int progress);

    void showConnectionStarting();

    void hideProgress();

    void setPersonas(ArrayList<HashMap<String, String>> personas);

    void showDetails(Persona persona);
}
