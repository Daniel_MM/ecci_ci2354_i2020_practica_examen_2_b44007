package cr.ac.ucr.ecci.eseg.miexamen02.main.presenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.main.MainActivityView;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.LazyAdapter;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.model.Persona;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.interactor.PersonasInteractor;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.interactor.PersonasInteractorImpl;

public class MainActicityPresenterImpl implements MainActivityPresenter {

    private MainActivityView mainActivityView;
    private PersonasInteractor personasInteractor = new PersonasInteractorImpl();
    private List<Persona> personas;

    public MainActicityPresenterImpl(MainActivityView mainActivityView) {
        this.mainActivityView = mainActivityView;
        ((PersonasInteractorImpl) personasInteractor).setOnProgressUpdateListener(this);
        ((PersonasInteractorImpl) personasInteractor).setOnFinishedListener(this);
    }

    @Override
    public void onResume() {
        if (mainActivityView != null) {
            mainActivityView.showProgress();
        }
        personasInteractor.getItems();
    }

    @Override
    public void onItemClicked(int position) {
        if (mainActivityView != null) {
//            String message = "position " + (position + 1) + " tapped";
//            mainActivityView.showMessage(message);
            mainActivityView.showDetails(personas.get(position));
        }
    }

    @Override
    public void onDestroy() {
        mainActivityView = null;
    }

    @Override
    public void onFinished(List<Persona> personas) {
        this.personas = personas;
        if (mainActivityView != null) {
            ArrayList<HashMap<String, String>> listaPersonas = new ArrayList<>();
            for (Persona persona : personas) {
                HashMap<String, String> map = new HashMap<>();
                map.put(LazyAdapter.NOMBRE, persona.getNombre());
                map.put(LazyAdapter.IDENTIFICACION, persona.getIdentificacion());
                listaPersonas.add(map);
            }

            mainActivityView.setPersonas(listaPersonas);
            mainActivityView.hideProgress();
        }
    }

    @Override
    public void onProgressUpdate(int value) {
        if (value < 0) {
            mainActivityView.showConnectionStarting();
        } else {
            mainActivityView.updateProgress(value);
        }
    }
}