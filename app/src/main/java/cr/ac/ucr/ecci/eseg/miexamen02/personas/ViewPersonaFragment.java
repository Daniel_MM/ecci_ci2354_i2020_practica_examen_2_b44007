package cr.ac.ucr.ecci.eseg.miexamen02.personas;

import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cr.ac.ucr.ecci.eseg.miexamen02.R;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.model.Persona;

public class ViewPersonaFragment extends Fragment implements View.OnClickListener {

    private static final String PERSONA = "persona";

    private Persona persona;
    private View view;

    public static ViewPersonaFragment newInstance(Persona persona) {
        ViewPersonaFragment fragment = new ViewPersonaFragment();
        Bundle args = new Bundle();
        args.putParcelable(PERSONA, persona);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            persona = getArguments().getParcelable(PERSONA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_persona_display, container, false);

        TextView nombre = view.findViewById(R.id.nombre);
        TextView identificacion = view.findViewById(R.id.identificacion);
        ImageButton back = view.findViewById(R.id.back);

        nombre.setText(persona.getNombre());
        identificacion.setText(persona.getIdentificacion());

        back.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(requireContext(),R.layout.fragment_view_persona_exit);
        ConstraintLayout constraintLayout = view.findViewById(R.id.fragment_constraint_layout);

        TransitionManager.beginDelayedTransition(constraintLayout);

        constraintSet.applyTo(constraintLayout);

        FragmentManager manager = requireActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.remove(this).commit();

        requireActivity().findViewById(R.id.list).setVisibility(View.VISIBLE);
    }
}