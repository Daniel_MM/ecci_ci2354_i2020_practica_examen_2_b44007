package cr.ac.ucr.ecci.eseg.miexamen02.main;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcherOwner;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.HashMap;

import cr.ac.ucr.ecci.eseg.miexamen02.R;
import cr.ac.ucr.ecci.eseg.miexamen02.main.presenter.MainActicityPresenterImpl;
import cr.ac.ucr.ecci.eseg.miexamen02.main.presenter.MainActivityPresenter;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.LazyAdapter;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.ViewPersonaFragment;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.model.Persona;

public class MainActivity extends AppCompatActivity implements
        MainActivityView, AdapterView.OnItemClickListener {

    private ListView listView;
    private ProgressBar progressBar;
    private TextView progressText;
    private MainActivityPresenter mainActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.list);
        listView.setOnItemClickListener(this);

        progressBar = findViewById(R.id.progress_bar);
        progressText = findViewById(R.id.progress_text);

        mainActivityPresenter = new MainActicityPresenterImpl(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mainActivityPresenter.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainActivityPresenter.onDestroy();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        progressText.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
    }

    @Override
    public void updateProgress(int progress) {
        progressBar.setProgress(progress);
        String message = progress + "/" + progressBar.getMax();
        progressText.setText(message);
    }

    @Override
    public void showConnectionStarting() {
        progressText.setText(R.string.conectando);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        progressText.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setPersonas(ArrayList<HashMap<String, String>> personas) {
        LazyAdapter adapter = new LazyAdapter(this, personas);
        listView.setAdapter(adapter);
    }

    @Override
    public void showDetails(Persona persona) {
        listView.setVisibility(View.GONE);

        FragmentManager manager = getSupportFragmentManager();
        ViewPersonaFragment fragment = ViewPersonaFragment.newInstance(persona);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.data, fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listView.setItemChecked(position, true);
        mainActivityPresenter.onItemClicked(position);
    }
}