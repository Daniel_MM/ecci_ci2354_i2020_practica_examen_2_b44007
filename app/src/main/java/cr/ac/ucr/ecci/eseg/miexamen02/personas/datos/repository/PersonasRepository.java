package cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.repository;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.exceptions.CantRetrieveItemsException;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.model.Persona;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.service.ServiceDataSource;

public interface PersonasRepository extends
        ServiceDataSource.OnProgressUpdateListener, ServiceDataSource.OnFinishedListener {

    interface OnProgressUpdateListener {
        void onProgressUpdate(int progress);
    }

    interface OnFinishedListener {
        void onFinished(List<Persona> personas);
    }

    void obtainItems() throws CantRetrieveItemsException;
}
