package cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.service;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.exceptions.BaseDataItemsException;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.model.Persona;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.model.Personas;

public class ServiceDataSourceImpl extends AsyncTask<Void, Float, List<Persona>> implements
        ServiceDataSource {

    private static final String URL = "https://bitbucket.org/lyonv/ci0161_i2020_exii/raw/4250ce77dc7fdb5f5c1324d8c8d5bf5ff3959740/Personas28.json";

    OnProgressUpdateListener onProgressUpdateListener;
    OnFinishedListener onFinishedListener;

    public void setOnProgressUpdateListener(OnProgressUpdateListener onProgressUpdateListener) {
        this.onProgressUpdateListener = onProgressUpdateListener;
    }

    public void setOnFinishedListener(OnFinishedListener onFinishedListener) {
        this.onFinishedListener = onFinishedListener;
    }

    @Override
    public List<Persona> obtainItems() throws BaseDataItemsException {
        try {
            onProgressUpdateListener.onProgressUpdate(-1);
            URL url = new URL(URL);
            URLConnection urlConnection = url.openConnection();
            int total = urlConnection.getContentLength();
            InputStream mInputStream = (InputStream) urlConnection.getContent();
            InputStreamReader mInputStreamReader = new InputStreamReader(mInputStream);
            BufferedReader responseBuffer = new BufferedReader(mInputStreamReader);
            StringBuilder strBuilder = new StringBuilder();
            String line;

            int actual = -1;

            while ((line = responseBuffer.readLine()) != null) {
                strBuilder.append(line);
                actual += line.length() + 1;
                Thread.sleep((line.length() + 1));  // Delay para efectos demostrativos
                publishProgress((float) actual / total);
            }

            Gson mJson = new Gson();
            Personas personas = mJson.fromJson(strBuilder.toString(), Personas.class);
            return personas.getPersonas();
        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }
    }

    @Override
    protected List<Persona> doInBackground(Void... voids) {
        try {
            return obtainItems();
        } catch (BaseDataItemsException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Persona> result) {
        super.onPostExecute(result);
        onFinishedListener.onFinished(result);
    }

    @Override
    protected void onProgressUpdate(Float... values) {
        super.onProgressUpdate(values);
        int progress = Math.round(values[0] * 100);
        onProgressUpdateListener.onProgressUpdate(progress);
    }
}