package cr.ac.ucr.ecci.eseg.miexamen02.main.presenter;

import cr.ac.ucr.ecci.eseg.miexamen02.personas.interactor.PersonasInteractor;

public interface MainActivityPresenter extends
        PersonasInteractor.OnProgressUpdateListener, PersonasInteractor.OnFinishedListener {

    void onResume();

    void onItemClicked(int position);

    void onDestroy();
}