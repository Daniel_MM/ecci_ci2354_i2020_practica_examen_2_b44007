package cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.service;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.exceptions.BaseDataItemsException;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.model.Persona;

public interface ServiceDataSource {

    interface OnProgressUpdateListener {
        void onProgressUpdate(int progress);
    }

    interface OnFinishedListener {
        void onFinished(List<Persona> personas);
    }

    List<Persona> obtainItems() throws BaseDataItemsException;
}
