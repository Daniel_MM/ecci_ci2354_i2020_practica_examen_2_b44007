package cr.ac.ucr.ecci.eseg.miexamen02.personas.interactor;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.model.Persona;
import cr.ac.ucr.ecci.eseg.miexamen02.personas.datos.repository.PersonasRepository;

public interface PersonasInteractor extends
        PersonasRepository.OnProgressUpdateListener, PersonasRepository.OnFinishedListener {

    interface OnFinishedListener {
        void onFinished(List<Persona> personas);
    }

    interface OnProgressUpdateListener {
        void onProgressUpdate(int value);
    }

    void getItems();
}
