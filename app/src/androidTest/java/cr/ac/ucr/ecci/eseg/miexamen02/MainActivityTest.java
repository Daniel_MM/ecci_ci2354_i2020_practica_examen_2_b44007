package cr.ac.ucr.ecci.eseg.miexamen02;



import android.content.Context;


import androidx.test.core.app.ApplicationProvider;

import org.junit.Test;

import static org.junit.Assert.*;

public class MainActivityTest {

    @Test
    public void testContext() throws Exception{

        Context context = ApplicationProvider.getApplicationContext();
        assertEquals("cr.ac.ucr.ecci.eseg.miexamen02", context.getPackageName());
    }

}